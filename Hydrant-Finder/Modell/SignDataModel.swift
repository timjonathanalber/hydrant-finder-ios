//
//  SignDataModel.swift
//  Hydrant-Finder
//
//  Created by tim alber on 03.10.20.
//  Copyright © 2020 tim alber. All rights reserved.
//

import Foundation
struct SignDataModel{
    var leftOrRight: String;
    var leftOrRightDistance: Double;
    var forwardDistance: Double;
    
    init(leftOrRight: String, leftOrRightDistance: Double, forwardDistance: Double) {
        self.leftOrRight = leftOrRight
        self.leftOrRightDistance = leftOrRightDistance
        self.forwardDistance = forwardDistance
    }
}
