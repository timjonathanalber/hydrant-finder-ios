//
//  GridMaterial.swift
//  Hydrant-Finder
//
//  Created by tim alber on 14.10.20.
//  Copyright © 2020 tim alber. All rights reserved.
//

import Foundation
import ARKit

class GridMaterial: SCNMaterial {
    
    func updateWith(anchor: ARPlaneAnchor) {
        let mmPerMeter: Float = 1000
        let mmOfImage: Float = 65
        let repeatAmount: Float = mmPerMeter / mmOfImage

        diffuse.contentsTransform = SCNMatrix4MakeScale(anchor.extent.x * repeatAmount, anchor.extent.z * repeatAmount, 1)
    }

    override init() {
        super.init()
        // 1
        let image = UIImage(named: "Grid")

        // 2
        diffuse.contents = image
        diffuse.wrapS = .repeat
        diffuse.wrapT = .repeat
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
