//
//  ARSceneManager.swift
//  Hydrant-Finder
//
//  Created by tim alber on 14.10.20.
//  Copyright © 2020 tim alber. All rights reserved.
//

import Foundation
import ARKit

class ARSceneManager: NSObject, ARSCNViewDelegate {
    var sceneView: ARSCNView?
    private var planes = [UUID: Plane]()
    
    func attach(to sceneView: ARSCNView) {
        self.sceneView = sceneView
        self.sceneView?.delegate = self
        
        configureSceneView(self.sceneView!)
    }
    
    func displayDegubInfo() {
        sceneView?.showsStatistics = true
        sceneView?.debugOptions = [] // ARSCNDebugOptions.showFeaturePoints
    }
    
    private func configureSceneView(_ sceneView: ARSCNView) {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .vertical //[.vertical, .horizontal]
        configuration.isLightEstimationEnabled = true

        sceneView.session.run(configuration)
    }
}

extension ARSceneManager {

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        print("Found plane: \(planeAnchor)")
        
        let plane = Plane(anchor: planeAnchor)
        planes[anchor.identifier] = plane
        node.addChildNode(plane)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }

        if let plane = planes[planeAnchor.identifier] {
            plane.updateWith(anchor: planeAnchor)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        planes.removeValue(forKey: anchor.identifier)
    }

}
