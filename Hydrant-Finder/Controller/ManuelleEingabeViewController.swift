//
//  ManuelleEingabeViewController.swift
//  Hydrant-Finder
//
//  Created by tim alber on 03.10.20.
//  Copyright © 2020 tim alber. All rights reserved.
//

import UIKit

class ManuelleEingabeViewController: UIViewController, UITextFieldDelegate{
    @IBOutlet weak var leftTextField: UITextField!
    @IBOutlet weak var rightTextField: UITextField!
    @IBOutlet weak var bottumTextField: UITextField!
    
    var signData: SignDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leftTextField.addTarget(self, action: #selector(leftTextFieldDidChange(_:)), for: .editingDidEnd)
        rightTextField.addTarget(self, action: #selector(rightTextFieldDidChange(_:)), for: .editingDidEnd)
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    @objc func leftTextFieldDidChange(_ textField: UITextField) {
        rightTextField.text = "";
    }
    
    @objc func rightTextFieldDidChange(_ textField: UITextField) {
        leftTextField.text = "";
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func Okbuttondidtap(_ sender: Any) {
        if(!rightTextField.text!.isEmpty && !leftTextField.text!.isEmpty){
            let alert = UIAlertController(title: "Das macht so keinen Sinn", message: "Es kann nur rechts oder link angegeben werden, nicht beides.", preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
        } else {
            signData = getSignData()
            //print(signData!.leftOrRight, signData!.leftOrRightDistance, signData!.forwardDistance)
            
//            if let storyboard = self.storyboard {
//                let vc = storyboard.instantiateViewController(withIdentifier: "armodevc") as! ARModeViewController
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: false, completion: nil)
//            }
            performSegue(withIdentifier: "showAR", sender: nil)
        }
    }
    
    func getSignData() -> SignDataModel{
        var leftOrRight = "right"
        var distance = 0.0
        var forwardDistance = 0.0
        if(!rightTextField.text!.isEmpty){
            leftOrRight = "right"
            if (rightTextField.text!.isNumber) {
                distance = (rightTextField.text?.toDoubleNumber)!
            } else {
                showPopup()
            }
        } else {
            leftOrRight = "left"
            if (leftTextField.text!.isNumber){
                if let leftdistance = leftTextField.text!.toDoubleNumber{
                    distance = leftdistance
                } else {
                    distance = 0.0
                }
            } else {
                showPopup()
            }
        }
        if (bottumTextField.text!.isNumber) {
            if let fordistance = bottumTextField.text!.toDoubleNumber{
                forwardDistance = fordistance
            } else {
                forwardDistance = 0.0
            }
        } else {
            showPopup()
        }
        
        return SignDataModel(leftOrRight: leftOrRight, leftOrRightDistance: distance, forwardDistance: forwardDistance)
    }
    
    func showPopup(){
        let alert = UIAlertController(title: "Ungültige Eingabe", message: "Nur Zahlen und ein Punkt sind erlaubt.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showAR") {
            let vc = segue.destination as! ARModeViewController
            vc.leftOrRight = signData?.leftOrRight
            vc.leftOrRightDistance = signData?.leftOrRightDistance
            vc.forwardDistance = signData?.forwardDistance
        }
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension String  {
    var isNumber: Bool {
        let formatter = NumberFormatter()
        if (self.contains(",")){
            formatter.decimalSeparator = ","
        } else if(self.contains(".")){
            formatter.decimalSeparator = "."
        }
        let grade = formatter.number(from: self)
        
        if (self.isEmpty){
            return true
        }

        if let doubleGrade = grade?.doubleValue {
            return true
        }
        return false
    }
    
    var toDoubleNumber: Double? {
        let formatter = NumberFormatter()
        if (self.contains(",")){
            formatter.decimalSeparator = ","
        } else if(self.contains(".")){
            formatter.decimalSeparator = "."
        }
        
        let grade = formatter.number(from: self)

        if let doubleGrade = grade?.doubleValue {
            return doubleGrade
        }
        return nil
    }
}
