//
//  ARModeViewController.swift
//  Hydrant-Finder
//
//  Created by tim alber on 14.10.20.
//  Copyright © 2020 tim alber. All rights reserved.
//

import UIKit
import ARKit

class ARModeViewController: UIViewController {
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var undoBtnOutlet: UIButton!
    
    
    let sceneManager = ARSceneManager()
    
    var leftOrRight: String?
    var leftOrRightDistance: Double?
    var forwardDistance: Double?

    var signArrow: SCNNode?
    var firstNode: SCNNode?
    var secondNode: SCNNode?
    var firstLine: SCNNode?
    var secondLine: SCNNode?
    
    var placed = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneManager.attach(to: sceneView)
        sceneManager.displayDegubInfo()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapScene(_:)))
        view.addGestureRecognizer(tapGesture)
        
        undoBtnOutlet.isHidden = true;
        
        print(leftOrRight, leftOrRightDistance, forwardDistance)
        
    }
    
    @IBAction func undoBtnTapped(_ sender: Any) {
        signArrow!.removeFromParentNode()
        firstNode!.removeFromParentNode()
        secondNode!.removeFromParentNode()
        firstLine!.removeFromParentNode()
        secondLine!.removeFromParentNode()
        placed = false;
        undoBtnOutlet.isHidden = true;
    }
    
    
    @objc func didTapScene(_ gesture: UITapGestureRecognizer) {
        switch gesture.state {
        case .ended:
            let location = gesture.location(ofTouch: 0,
                                            in: sceneView)
            let hit = sceneView.hitTest(location,
                                        types: .existingPlaneUsingGeometry)
            if !placed{
                if let hit = hit.first {
                    placeBlockOnPlaneAt(hit)
                    drawfirstNode()
                    drawsecondNode()
                    placed = true;
                    undoBtnOutlet.isHidden = false;
                }
            }
        default:
            print("tapped default")
        }
    }
    
    func drawfirstNode(){
        let position = SCNVector3(x: 0, y: 0, z: Float(forwardDistance!) * -1)
        firstNode = createSecondBox()
        sceneView?.scene.rootNode.addChildNode(firstNode!)
        updatePositionAndOrientationOf(firstNode!, withPosition: position, relativeTo: signArrow!)
        
        firstLine = SCNNode().buildLineInTwoPointsWithRotationForDistance(from: signArrow!.worldPosition, to: firstNode!.worldPosition, radius: 0.01, color: .green)
        sceneView.scene.rootNode.addChildNode(firstLine!)
    }
    
    func drawsecondNode(){
        var position = SCNVector3()
        if (leftOrRight == "right"){
            position = SCNVector3(x: Float(leftOrRightDistance!), y: 0, z: 0)
        } else {
            position = SCNVector3(x: Float(leftOrRightDistance!) * -1, y: 0, z: 0)
        }
        
        secondNode = createThirdBox()
        sceneView?.scene.rootNode.addChildNode(secondNode!)
        updatePositionAndOrientationOf(secondNode!, withPosition: position, relativeTo: firstNode!)
        
        secondLine = SCNNode().buildLineInTwoPointsWithRotationForDistance(from: firstNode!.worldPosition, to: secondNode!.worldPosition, radius: 0.01, color: .green)
        sceneView.scene.rootNode.addChildNode(secondLine!)
    }
    
    func updatePositionAndOrientationOf(_ node: SCNNode, withPosition position: SCNVector3, relativeTo referenceNode: SCNNode) {
        let referenceNodeTransform = matrix_float4x4(referenceNode.transform)

        var translationMatrix = matrix_identity_float4x4
        translationMatrix.columns.3.x = position.x
        translationMatrix.columns.3.y = position.y
        translationMatrix.columns.3.z = position.z

        let updatedTransform = matrix_multiply(referenceNodeTransform, translationMatrix)
        node.transform = SCNMatrix4(updatedTransform)
    }
    
    func placeBlockOnPlaneAt(_ hit: ARHitTestResult) {
        let box = createBox()
        position(node: box, atHit: hit)
        signArrow = box;
        sceneView?.scene.rootNode.addChildNode(box)
        
        print("Quaternation: ", signArrow?.orientation)
    }
    
    private func createBox() -> SCNNode {
        let box = SCNBox(width: 0.05, height: 0.05, length: 0.05, chamferRadius: 0.01)
        box.firstMaterial?.diffuse.contents = UIColor.red
        let boxNode = SCNNode(geometry: box)
        boxNode.physicsBody = SCNPhysicsBody(type: .static, shape: SCNPhysicsShape(geometry: box, options: nil))
        
        return boxNode
    }
    
    private func createSecondBox() -> SCNNode {
        let box = SCNBox(width: 0.05, height: 0.05, length: 0.05, chamferRadius: 0.01)
        box.firstMaterial?.diffuse.contents = UIColor.blue
        let boxNode = SCNNode(geometry: box)
        boxNode.physicsBody = SCNPhysicsBody(type: .static, shape: SCNPhysicsShape(geometry: box, options: nil))
        
        return boxNode
    }
    
    private func createThirdBox() -> SCNNode {
        let box = SCNBox(width: 0.05, height: 0.05, length: 0.05, chamferRadius: 0.01)
        box.firstMaterial?.diffuse.contents = UIColor.yellow
        let boxNode = SCNNode(geometry: box)
        boxNode.physicsBody = SCNPhysicsBody(type: .static, shape: SCNPhysicsShape(geometry: box, options: nil))
        
        return boxNode
    }
    
    private func position(node: SCNNode, atHit hit: ARHitTestResult) {
        node.transform = SCNMatrix4(hit.anchor!.transform)
        node.eulerAngles = SCNVector3Make(node.eulerAngles.x + (Float.pi / 2), node.eulerAngles.y, node.eulerAngles.z)
        
        let position = SCNVector3Make(hit.worldTransform.columns.3.x + node.geometry!.boundingBox.min.z, hit.worldTransform.columns.3.y, hit.worldTransform.columns.3.z + 0.05)
        node.position = position
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

func normalizeVector(_ iv: SCNVector3) -> SCNVector3 {
    let length = sqrt(iv.x * iv.x + iv.y * iv.y + iv.z * iv.z)
    if length == 0 {
        return SCNVector3(0.0, 0.0, 0.0)
    }
    return SCNVector3( iv.x / length, iv.y / length, iv.z / length)
}

extension SCNNode {
    func buildLineInTwoPointsWithRotationForDistance(from startPoint: SCNVector3,
                                                     to endPoint: SCNVector3,
                                                     radius: CGFloat,
                                                     color: UIColor) -> SCNNode {
        let w = SCNVector3(x: endPoint.x-startPoint.x,
                           y: endPoint.y-startPoint.y,
                           z: endPoint.z-startPoint.z)
        let l = CGFloat(sqrt(w.x * w.x + w.y * w.y + w.z * w.z))
        
        if l == 0.0 {
            // two points together.
            let sphere = SCNSphere(radius: radius)
            sphere.firstMaterial?.diffuse.contents = color
            self.geometry = sphere
            self.position = startPoint
            return self
            
        }
        
        let cyl = SCNCylinder(radius: radius, height: l)
        cyl.firstMaterial?.diffuse.contents = color
        
        self.geometry = cyl
        self.opacity = 0.7
        //original vector of cylinder above 0,0,0
        let ov = SCNVector3(0, l/2.0,0)
        //target vector, in new coordination
        let nv = SCNVector3((endPoint.x - startPoint.x)/2.0, (endPoint.y - startPoint.y)/2.0,
                            (endPoint.z-startPoint.z)/2.0)
        // axis between two vector
        let av = SCNVector3( (ov.x + nv.x)/2.0, (ov.y+nv.y)/2.0, (ov.z+nv.z)/2.0)
        
        //normalized axis vector
        let av_normalized = normalizeVector(av)
        let q0 = Float(0.0) //cos(angel/2), angle is always 180 or M_PI
        let q1 = Float(av_normalized.x) // x' * sin(angle/2)
        let q2 = Float(av_normalized.y) // y' * sin(angle/2)
        let q3 = Float(av_normalized.z) // z' * sin(angle/2)
        
        let r_m11 = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3
        let r_m12 = 2 * q1 * q2 + 2 * q0 * q3
        let r_m13 = 2 * q1 * q3 - 2 * q0 * q2
        let r_m21 = 2 * q1 * q2 - 2 * q0 * q3
        let r_m22 = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3
        let r_m23 = 2 * q2 * q3 + 2 * q0 * q1
        let r_m31 = 2 * q1 * q3 + 2 * q0 * q2
        let r_m32 = 2 * q2 * q3 - 2 * q0 * q1
        let r_m33 = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3
        
        self.transform.m11 = r_m11
        self.transform.m12 = r_m12
        self.transform.m13 = r_m13
        self.transform.m14 = 0.0
        
        self.transform.m21 = r_m21
        self.transform.m22 = r_m22
        self.transform.m23 = r_m23
        self.transform.m24 = 0.0
        
        self.transform.m31 = r_m31
        self.transform.m32 = r_m32
        self.transform.m33 = r_m33
        self.transform.m34 = 0.0
        
        self.transform.m41 = (startPoint.x + endPoint.x) / 2.0
        self.transform.m42 = (startPoint.y + endPoint.y) / 2.0
        self.transform.m43 = (startPoint.z + endPoint.z) / 2.0
        self.transform.m44 = 1.0
        return self
    }
}
