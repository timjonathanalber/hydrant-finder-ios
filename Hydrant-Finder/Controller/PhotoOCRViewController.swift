//
//  PhotoTakeViewController.swift
//  Hydrant-Finder
//
//  Created by tim alber on 19.10.20.
//  Copyright © 2020 tim alber. All rights reserved.
//

import UIKit
import Firebase

class PhotoOCRViewController: UIViewController {
    
    @IBOutlet weak var debugImageView: UIImageView!
    @IBOutlet weak var leftTextField: UITextField!
    @IBOutlet weak var rightTextField: UITextField!
    @IBOutlet weak var buttomTextField: UITextField!
    @IBOutlet weak var OkayButton: UIButton!
    
    var photo: UIImage?
    
    var leftOrRight: String?
    var leftOrRightDistance: Double?
    var forwardDistance: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leftTextField.isEnabled = false
        rightTextField.isEnabled = false
        buttomTextField.isEnabled = false
        OkayButton.isEnabled = false
        
        debugImageView.image = photo
        
        let vision = Vision.vision()
        let textRecognizer = vision.onDeviceTextRecognizer()
        let image = VisionImage(image: photo!)
        var signData: [VisionTextElement] = []
        
        textRecognizer.process(image) { [self] result, error in
            guard error == nil, let result = result else {
                print("Got Error")
                return
            }
            
            for blocks in result.blocks{
                for line in blocks.lines{
                    for element in line.elements{
                        signData.append(element)
                    }
                }
            }
            
            if(signData.count < 2){
                print("Found no text")
            } else {
                signData.reverse()
                buttomTextField.text = signData[0].text
                forwardDistance = signData[0].text.toDoubleNumber
                leftOrRightDistance = signData[1].text.toDoubleNumber
                
                if(signData[1].frame.center.x < photo!.size.width/2){
                    leftOrRight = "left"
                    leftTextField.text = signData[1].text
                } else {
                    leftOrRight = "right"
                    rightTextField.text = signData[1].text
                }
                
                if(signData[0].text.isNumber && signData[0].text.isNumber){
                    OkayButton.isEnabled = true
                }
            }
        }
        
    }
    
    @IBAction func OkButtonDidTapped(_ sender: Any) {
        performSegue(withIdentifier: "goToARFromOCR", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToARFromOCR") {
            let vc = segue.destination as! ARModeViewController
            vc.leftOrRight = leftOrRight
            vc.leftOrRightDistance = leftOrRightDistance
            vc.forwardDistance = forwardDistance
        }
    }
    
}

extension CGRect {
    var center: CGPoint { return CGPoint(x: midX, y: midY) }
}
